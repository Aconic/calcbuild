


import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CalcFuncsTest
{
    @Test
     public void testCalc() throws Exception
{
    double res = CalcFuncs.calc(1, 2, "+");
    assertEquals(3.0,res);
}
    @Test
    public void testCalc_divide() throws Exception
    {
        double res = CalcFuncs.calc(3, 2, "/");
        assertEquals(1.5,res);
    }
}
