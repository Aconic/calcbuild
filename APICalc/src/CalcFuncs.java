

public class CalcFuncs
{
        public static double calc(int a, double b, String op)
        {
            double res = 0;
            switch (op)
            {
                case "+":
                    res = a + b;
                    break;
                case "-":
                    res = a - b;
                    break;
                case "/":
                    res = a / b;
                    break;
                case "*":
                    res = a * b;
                    break;
                default:break;
            }
            return res;
        }
}
